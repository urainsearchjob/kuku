FROM nginx:latest
COPY ./index.html /usr/share/nginx/html/index.html
RUN mkdir /usr/share/nginx/html/css
COPY ./css /usr/share/nginx/html/css
RUN mkdir /usr/share/nginx/html/img
COPY ./img /usr/share/nginx/html/img
RUN mkdir /usr/share/nginx/html/js
COPY ./js /usr/share/nginx/html/js
